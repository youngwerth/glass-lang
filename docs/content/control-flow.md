# Glass Language Control Flow

- [Glass Language Control Flow](#glass-language-control-flow)
- [If](#if)
    - [Else](#else)
    - [Else if](#else-if)
    - [Ifs are expressions](#ifs-are-expressions)
- [Match](#match)
    - [Matches are expressions](#matches-are-expressions)
- [Loops](#loops)
    - [For loops](#for-loops)
    - [While loops](#while-loops)
    - [Inifinite loops](#inifinite-loops)

# If

An `if` is a single expression followed by a function with no parameters.

If the expression evaluates to true it executes the subsequent function,
otherwise it does nothing.

Example:
```rust
let x = 5;

if x == 5 {
    print("X equals five")
}

// Result:
// X equals five
```

## Else

Ifs can also be followed be an `else` statement

Example:
```rust
let x = 3

if x === 5 {
    print("X equals five")
} else {
    print("X does not equal five")
}
```

## Else if

You can also chain ifs together with `else if` statements

Example:
```rust
let x = 10

if x === 5 {
    print("X equals five")
} else if x > 5 {
    print("X is greater than five")
} else if x < 5 {
    print("X is less than five")
} else {
    print("X does not equal five")
}
```

## Ifs are expressions

Ifs themselves are an expression. An `if` always return the result of the last
expression in their function. In the above examples they all returned `Void`
and the value was ignored. But you can assign a variable to the result of the
if statement.

Example:
```rust
let x = 4

let ten = if x == 2 { 2 } else { 10 }
```

# Match

A `match` in glass is is similar to a switch in other languages.

Matches take a value and a body with cases seperated by a colon and a comma.
Matches must always have a `default` case.

Matches can test against ints, strings, and enums.

Example:

```rust
let letter = "A"

match letter {
    "A": print("The letter A"),
    "B": print("The letter B"),
    "C": print("The letter C"),
    default: print("The letter UNKOWN")
}
```

## Matches are expressions

Like ifs, matches are expressions and reutn the value of the case expression.

Example:
```rust
let num = 2

let numName: String = match num {
    1: "one",
    2: "two",
    3: "three",
    4: "four",
    default: print("Unkown")
}

print(numName) // two
```

# Loops

## For loops

```rust
// For in (Iterates over a type that implements the iterable protocol)
let omlette = List("eggs", "bacon", "mushrooms", "spinach")
for item in omlette {
    print(item)
}
```

## While loops

```rust
var x = 1
while x < 25 {
    if x < 0 { break } // Supports `break` and `continue` statements
    x += 1
}
```

## Inifinite loops

```rust
loop { /* ... */ }
```
