```scala
/* Methods */
def foo { x: Int -> Int | match(x) {

}};

// Multiline
def foo { x: Int -> Int |
    let y = 3
    x * 2
}
```