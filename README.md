# Glass

The scalable language for high performance applications.

## Overview

Glass is, at this point, just an evolving language concept.

**Key goals:**
- Fast
- Readable
- Scalable

**Features:**
- Functional, Expressionm and Protocol Oriented
- Statically typed
- Generics
- Rust-like memory ownership
- Type inference

**Inspired by:**
- Rust
- Swift
- Scala
- Typescript

## Built-in Types

- Int
- String
- Float
- Double
- Number
- List
- Bool
- Dictionary

## Keywords

- async
- await
- break
- continue
- default
- enum
- extension
- else
- for
- guard
- if
- in
- inout
- let
- loop
- match
- mutating
- object
- owned
- protocol
- return
- recall
- shared
- struct
- type
- var
- while

## Comments

```rust
// Single line comment

/*
    Block Comment
*/
```

## Assignment

```rust
// Constant assignment
let x = 5

// Variable assignment
var y = 7

// With type annotations
let x: Int = 5
let y: Number = 8
```

## Value Types

```rust
// True boolean
true

// False boolean
false

// Int
3

// Double
3.0

// Fixed String - string (Fast)
'Hello World'

// Varialble sized String - String (Slower)
"Hello World!"

// Tuple
(1, "Hello", true)

// Labled Tuple
(x: 1, y: 2, z: 3)

// List
List(1, 2, 3)

// Map
Map(
    ("key", "value"),
    ("key2", "value2")
)

// nil
nil
```

## Basic Control flow

If statements:
```rust
if 5 > 3 {
    print("Five is greater than three")
}

// If else
if 5 < 3 {
    print("Five is less than three")
} else {
    print("Five is greater than three")
}

// If statements are expressions that return the result of the last expression
let info: String = if (3 > 2) {
    "Three greater than two"
} else {
    "Three not greater than two"
}

print(info) // "Three greater than two"
```

Match statements:
```rust
let num = 4
match num {
    1: print("one"),
    2: print("two"),
    3: print("three"),
    4: print("four"), // Executes
    default: print("Unkown")
}

// Match statements are expressions, just like if statements
let numName: String = match num {
    1: "one",
    2: "two",
    3: "three",
    4: "four",
    default: print("Unkown")
}

print(numName) // "Four"
```

Loops:

```rust
// While (Loops until condition evals to false)
var x = 1
while x < 25 {
    if x < 0 { break } // Supports `break` and `continue` statements
    x += 1
}

// For in (Iterates over an iterable value)
let omlette = List("eggs", "bacon", "mushrooms", "spinach")
for item in omlette {
    print(item)
}

// Loop. Loops until broken
loop { /* ... */ }

// Note, unlike ifs and matches, loops can only return Void (or ())
```

## Functions

A function is any set of expressions grouped between curly `{}` braces.
A function ALWAYS returns the result of the last expression.

```rust
// Basic example
{ 2 * 4 } // Always returns 8

// With parameters and annotated return type
{ x: Int -> Int |
    let y = 3
    x * 2
}

// Named
let foo = { x: Int -> Int | x * 2 }
let bar = { x: Int -> Int |
    let y = 3
    x * 2
}
```

## Methods

A method is a named function with a few added features
- Can have an explicit `return`
- Can be have multiple implementations with different function signatures
- Can define a generic type

```scala
def foo { x: Int -> Int | x * 2 }

// Multiline
def foo { x: Int -> Int |
    let y = 3
    x * 2
}
```

## Enums

```rust
enum StopLight {
    red, yellow, green
}

// Enum with associated values
enum VendingMachine {
    chips(Chip),
    soda(Soda),
    candy_bar(CandyBar)
}
```

## Structs

```rust
struct Point {
    var x: Int // You can also use "let" to make x immutable
    var y: Int
    var z: Int
}

// Init a point
let location = Point(x: 1, y: 2, z: 3)
```

## Objects

Objects are similar to structs with a few key differences.

- They must definie an init method
- They are passed by reference, not value (not coppied)
- Mutating methods do not need to be marked as "mutating

```scala
object Robot {
    let socket: Socket;

    init { ip: String |
        socket = Socket(ip)
    }

    def moveL { _ target: Target -> Promise | socket.send(buffer: target.toBuffer()) }

    def jog { axis: Axis, distance: Double -> Promise |
        let pos = await curPos()
        match axis {
            .x: moveL(pos.offs(distance, 0, 0))
            .y: moveL(pos.offs(0, distance, 0))
            .z: moveL(pos.offs(0, 0, distance))
        }
    }

    def curPos { () -> Target |  /* ... */ }
}

// Init an object
let fanuc = Robot(ip: "192.168.1.12")
```

## Protocol

A protocol defines all of the functions and variables that an object or struct
must implement

```scala
protocol Bakery {
    var cakes_baked: Int // Changeable
    let name: String // Fixed
    def make_cake (type: BreadType) -> Cake
    def make_bread (type: BreadType) -> Bread
    def make_tomato () -> Tomato
}
```

## Generics

```scala
// Generic add function
def add[T] { _ a: T, _ b: T -> T |
    return a + b
}

// Generic add with type constraint
def add[T: Addable] { _ a: T, _ b: T -> T |
    return a + b
}

// Generic struct
struct Vec3D[T] {
    let x: T
    let y: T
    let z: T
}
```

## Other Syntactic Sugar

```scala
/** Apply Shorthand **/

// Function calls like this:
func(5)

// Are shorthand for
func.invoke(5)

// So on an object or struct you could define an "apply" method like this:
struct MyArray[T] {
    let store = List[T]()

    def invoke { _ index: Int |
        store(index)
    }
}

/** Parameter shorthand **/

// A parameter label can be skipped if the item passed has the same name as the label.
let fib = { n: Int -> Int | /* ... */ }
let n = 7
let num = fib(n)
```