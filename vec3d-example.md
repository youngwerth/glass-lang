```ruby
import Foundation

struct Vec3D
    var x: Double
    var y: Double
    var z: Double
end

impl Vec3D
    def squaredLength()
        return x * x + y * y + z * z
    end

    def length()
        return sqrt(squaredLength)
    end

    mutating def makeUnitVector()
        let k = 1.0 / length
        x *= k; y *= k; z *= k;
    end

    prefix def - (vec: Vec3) -> Vec3
        return Vec3(-vec.x, -vec.y, -vec.z)
    end

    def + (left: Vec3, right: Vec3) -> Vec3
        return Vec3(left.x + right.x, left.y + right.y, left.z + right.z)
    end

    def - (left: Vec3, right: Vec3) -> Vec3
        return Vec3(left.x - right.x, left.y - right.y, left.z - right.z)
    end

    def * (left: Vec3, right: Vec3) -> Vec3
        return Vec3(left.x * right.x, left.y * right.y, left.z * right.z)
    end

    def / (left: Vec3, right: Vec3) -> Vec3
        return Vec3(left.x / right.x, left.y / right.y, left.z / right.z)
    end

    def * (left: Double, right: Vec3) -> Vec3
        return Vec3(left * right.x, left * right.y, left * right.z)
    end

    def * (left: Vec3, right: Double) -> Vec3
        return Vec3(left.x * right, left.y * right, left.z * right)
    end

    def / (left: Vec3, right: Double) -> Vec3
        return Vec3(left.x / right, left.y / right, left.z / right)
    end

    def += (left: inout Vec3, right: Vec3)
        left = left + right
    end

    def -= (left: inout Vec3, right: Vec3)
        left = left - right
    end

    def *= (left: inout Vec3, right: Vec3)
        left = left * right
    end

    def /= (left: inout Vec3, right: Vec3)
        left = left / right
    end

    def *= (left: inout Vec3, right: Double)
        left = left * right
    end

    def /= (left: inout Vec3, right: Double)
        let k = 1.0 / right
        left = left * k
    end
end
```