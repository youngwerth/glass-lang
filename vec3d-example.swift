import Foundation

struct Vec3 {
  var x: Double
  var y: Double
  var z: Double
  var r: Double {
    get { return x }
    set(v) { x = v }
  }
  var g: Double {
    get { return y }
    set(v) { y = v }
  }
  var b: Double {
    get { return z }
    set(v) { z = v }
  }

  var squaredLength: Double { return x * x + y * y + z * z }
  var length: Double { return sqrt(squaredLength) }

  mutating func makeUnitVector() {
    let k = 1.0 / length
    x *= k; y *= k; z *= k;
  }
}

extension Vec3 {
  init(_ x: Double, _ y: Double, _ z: Double) {
    self.init(x: x, y: y, z: z)
  }

  init(r x: Double, g y: Double, b z: Double) {
    self.init(x: x, y: y, z: z)
  }
}

extension Vec3 {
  static prefix func - (vec: Vec3) -> Vec3 {
    return Vec3(-vec.x, -vec.y, -vec.z)
  }

  static func + (left: Vec3, right: Vec3) -> Vec3 {
    return Vec3(left.x + right.x, left.y + right.y, left.z + right.z)
  }

  static func - (left: Vec3, right: Vec3) -> Vec3 {
    return Vec3(left.x - right.x, left.y - right.y, left.z - right.z)
  }

  static func * (left: Vec3, right: Vec3) -> Vec3 {
    return Vec3(left.x * right.x, left.y * right.y, left.z * right.z)
  }

  static func / (left: Vec3, right: Vec3) -> Vec3 {
    return Vec3(left.x / right.x, left.y / right.y, left.z / right.z)
  }

  static func * (left: Double, right: Vec3) -> Vec3 {
    return Vec3(left * right.x, left * right.y, left * right.z)
  }

  static func * (left: Vec3, right: Double) -> Vec3 {
    return Vec3(left.x * right, left.y * right, left.z * right)
  }

  static func / (left: Vec3, right: Double) -> Vec3 {
    return Vec3(left.x / right, left.y / right, left.z / right)
  }

  static func += (left: inout Vec3, right: Vec3) {
    left = left + right
  }

  static func -= (left: inout Vec3, right: Vec3) {
    left = left - right
  }

  static func *= (left: inout Vec3, right: Vec3) {
    left = left * right
  }

  static func /= (left: inout Vec3, right: Vec3) {
    left = left / right
  }

  static func *= (left: inout Vec3, right: Double) {
    left = left * right
  }

  static func /= (left: inout Vec3, right: Double) {
    let k = 1.0 / right
    left = left * k
  }
}
